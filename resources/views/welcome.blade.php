<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
        <link rel="manifest" href="/favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <title>Azul Anaité</title>

        <!-- DC Meta tags -->
        <meta name="DC.language" content="es"/>
        <meta name="DC.title" content="Azul Anaité"/>
        <meta name="DC.keywords" content="Azul Anaité"/>
        <meta name="DC.description" content="Azul Anaité es mujer ceremonialista y ritualista, guardiana de lo sagrado femenino, comparte talleres, círculos y formaciones para mujeres que deseen despertar su magia, encontrando los dones y regalos escondidos en su historia, así como recordar el camino hacia tomar a la naturaleza como guía en la vida."/>
        <!--  -->

        <!-- Twitter Card data -->
        <meta name="twitter:card" value="summary">
        <meta name="twitter:site" content="">
        <meta name="twitter:title" content="Azul Anaité">
        <meta name="twitter:description" content="Azul Anaité es mujer ceremonialista y ritualista, guardiana de lo sagrado femenino, comparte talleres, círculos y formaciones para mujeres que deseen despertar su magia, encontrando los dones y regalos escondidos en su historia, así como recordar el camino hacia tomar a la naturaleza como guía en la vida.">
        <meta name="twitter:creator" content="@fraque_sistemas">
        <meta name="twitter:url"  content="{{ url('/') }}">
        <meta name="twitter:image" content="{{ url('/web/home/img/mujer01_mision1.png') }}">
        <!--  -->

        <!-- Meta OG tags -->
        <meta property="og:title" content="Azul Anaité" />
        <meta property="og:site_name" content="Azul Anaité" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content=" {{ url('/') }}" />
        <meta property="og:image" content="{{ url('/web/home/img/mujer01_mision1.png') }}" />
        <meta property="og:description" content="Azul Anaité es mujer ceremonialista y ritualista, guardiana de lo sagrado femenino, comparte talleres, círculos y formaciones para mujeres que deseen despertar su magia, encontrando los dones y regalos escondidos en su historia, así como recordar el camino hacia tomar a la naturaleza como guía en la vida." />
        <meta property="fb:admins" content="198599390930033" />
        <!--  -->

        <!-- Styles -->
        <style type="text/css" media="screen,projection">
        /* GENERAL FONTS */
          @import url("/fonts/stylesheet.css");
        /*  */
        .error {
          width: 100%;
          height: auto;
          padding: 5px 0 0 0;
          color: red;
          transition: all .2s ease;
        }
        </style>
        <link type="text/css" rel="stylesheet"   href="/css/animate.css" media="screen,projection"/>
        <link type="text/css" rel="stylesheet"   href="/css/materialize.min.css" media="screen,projection"/>
        <link type="text/css" rel="stylesheet"   href="/css/sweetalert2.min.css" media="screen,projection"/>
        <link type="text/css" rel="stylesheet"   href="/css/owl.carousel.min.css" media="screen,projection"/>
        <link type="text/css" rel="stylesheet"   href="/css/owl.theme.default.min.css" media="screen,projection"/>

        <link rel="stylesheet" href="/web/navbar/style.min.css?var=<?php echo rand() ?>" media="screen,projection"/>
        <link rel="stylesheet" href="/web/navbar/responsive.min.css?var=<?php echo rand() ?>" media="screen,projection"/>

        <link rel="stylesheet" href="/web/home/style.min.css?var=<?php echo rand() ?>" media="screen,projection"/>
        <link rel="stylesheet" href="/web/home/responsive.min.css?var=<?php echo rand() ?>" media="screen,projection"/>
        <!--  -->

    </head>
    <body>
            <!-- MENU FIXED PC -->
            <div id="menuLeft" class="show-on-large hide-on-med-and-down">
                <div class="menuContainer">
                    <ul>
                        <li>
                            <a href="/">
                                <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 456.41">
                                    <title>Inicio</title>
                                    <path d="M506.55,208.06,263.86,30.37a13.3,13.3,0,0,0-15.72,0L5.45,208.06a13.3,13.3,0,0,0,15.72,21.47L256,57.59,490.84,229.53a13.3,13.3,0,0,0,15.72-21.47Z" transform="translate(0 -27.8)"/>
                                    <path d="M442.25,232.54a13.3,13.3,0,0,0-13.3,13.3V457.59H322.52V342a66.52,66.52,0,0,0-133,0V457.6H83.06V245.85a13.3,13.3,0,0,0-26.61,0V470.9a13.3,13.3,0,0,0,13.3,13.3h133A13.3,13.3,0,0,0,216,471.94a10,10,0,0,0,.05-1V342a39.91,39.91,0,1,1,79.83,0V470.9a9.76,9.76,0,0,0,.05,1,13.3,13.3,0,0,0,13.25,12.28h133a13.3,13.3,0,0,0,13.3-13.3V245.85A13.3,13.3,0,0,0,442.25,232.54Z" transform="translate(0 -27.8)"/>
                                </svg>
                            </a>
                        </li>
                        <li>
                            <a href="/lang/en">ENG</a>
                        </li>
                        <li>
                            <a href="/lang/es">ESP</a>
                        </li>
                        <li>
                            <a href="#blog">
                                <svg id="Icons" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 484.32 332.11">
                                    <title>Servicios</title>
                                    <path d="M231.78,422.05H211.44a6.92,6.92,0,1,1,0-13.84h20.34a6.92,6.92,0,0,1,0,13.84Z" transform="translate(-13.84 -89.95)"/>
                                    <path d="M470.49,422.05H349a6.92,6.92,0,0,1,0-13.84h121.5a13.84,13.84,0,0,0,13.84-13.84V117.62a13.84,13.84,0,0,0-13.84-13.84h-429a13.84,13.84,0,0,0-13.84,13.84V394.38a13.84,13.84,0,0,0,13.84,13.84H163a6.92,6.92,0,0,1,0,13.84H41.51a27.68,27.68,0,0,1-27.68-27.68V117.62A27.68,27.68,0,0,1,41.51,89.95h429a27.68,27.68,0,0,1,27.68,27.68V394.38A27.68,27.68,0,0,1,470.49,422.05Z" transform="translate(-13.84 -89.95)"/>
                                    <path d="M300.56,422.05H280.22a6.92,6.92,0,0,1,0-13.84h20.34a6.92,6.92,0,0,1,0,13.84Z" transform="translate(-13.84 -89.95)"/>
                                    <path d="M442.81,387.46H69.19A20.76,20.76,0,0,1,48.43,366.7V145.3a20.76,20.76,0,0,1,20.76-20.76H442.81a20.76,20.76,0,0,1,20.76,20.76V366.7A20.76,20.76,0,0,1,442.81,387.46ZM69.19,138.38a6.92,6.92,0,0,0-6.92,6.92V366.7a6.92,6.92,0,0,0,6.92,6.92H442.81a6.92,6.92,0,0,0,6.92-6.92V145.3a6.92,6.92,0,0,0-6.92-6.92Z" transform="translate(-13.84 -89.95)"/>
                                    <path d="M171.17,387.46a6.92,6.92,0,0,1-4.89-11.81L315.94,226.06a6.92,6.92,0,0,1,9.64-.15L461.4,353.71a6.92,6.92,0,0,1-9.49,10.08L321,240.59,176.07,385.43A6.92,6.92,0,0,1,171.17,387.46Z" transform="translate(-13.84 -89.95)"/>
                                    <path d="M55.35,352.59a6.92,6.92,0,0,1-4.89-11.81L154,237.27a6.92,6.92,0,0,1,9.64-.15l77.62,73.06a6.92,6.92,0,1,1-9.38,10.17l-.1-.1L159,251.8,60.24,350.56A6.92,6.92,0,0,1,55.35,352.59Z" transform="translate(-13.84 -89.95)"/>
                                    <path d="M228.32,226.59a30.27,30.27,0,1,1,30.27-30.27A30.27,30.27,0,0,1,228.32,226.59Zm0-46.7a16.43,16.43,0,1,0,16.43,16.43A16.43,16.43,0,0,0,228.32,179.89Z" transform="translate(-13.84 -89.95)"/>
                                </svg>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 473.77">
                                    <title>Tienda En Linea</title>
                                    <path d="M214.68,402.83a45,45,0,1,0,45,45A45.08,45.08,0,0,0,214.68,402.83Zm0,64.91a19.89,19.89,0,1,1,19.89-19.89A19.91,19.91,0,0,1,214.68,467.74Z" transform="translate(0 -19.11)"/>
                                    <path d="M372.63,402.83a45,45,0,1,0,45,45A45.08,45.08,0,0,0,372.63,402.83Zm0,64.91a19.89,19.89,0,1,1,19.89-19.89A19.91,19.91,0,0,1,372.63,467.74Z" transform="translate(0 -19.11)"/>
                                    <path d="M383.72,165.76H203.57a12.57,12.57,0,0,0,0,25.14H383.72a12.57,12.57,0,1,0,0-25.14Z" transform="translate(0 -19.11)"/>
                                    <path d="M373.91,231H213.37a12.57,12.57,0,0,0,0,25.14H373.91a12.57,12.57,0,0,0,0-25.14Z" transform="translate(0 -19.11)"/>
                                    <path d="M506.34,109.74a25,25,0,0,0-19.49-9.26H95.28L87.37,62.1a25.17,25.17,0,0,0-14.61-18l-55.18-24a12.57,12.57,0,0,0-10,23.06l55.18,24,60.83,295.26A25.24,25.24,0,0,0,148.2,382.5H449.36a12.57,12.57,0,0,0,0-25.14H148.2l-7.4-35.92H451.69a25.24,25.24,0,0,0,24.62-20.07L511.48,130.7A25,25,0,0,0,506.34,109.74ZM451.69,296.3H135.62L100.46,125.63H486.85Z" transform="translate(0 -19.11)"/>
                                </svg>
                            </a>
                        </li>
                        <li>
                            <a href="#instagram">
                                <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 410.48">
                                    <title>Contáctanos</title>
                                    <path d="M372.29,50.76H28.83A28.86,28.86,0,0,0,0,79.59V285.64a28.86,28.86,0,0,0,28.83,28.83H51.1v76.71a12.08,12.08,0,0,0,20.62,8.54L157,314.48h17.1a12.08,12.08,0,0,0,0-24.16s-22.69,0-22.93,0a12,12,0,0,0-7.72,3.5L75.26,362s0-60.39-.06-60.7a12.07,12.07,0,0,0-12-11H28.83a4.68,4.68,0,0,1-4.67-4.67V79.59a4.68,4.68,0,0,1,4.67-4.67H372.29A4.68,4.68,0,0,1,377,79.59v79.06a12.08,12.08,0,0,0,24.16,0V79.59A28.86,28.86,0,0,0,372.29,50.76Z" transform="translate(0 -50.76)"/>
                                    <path d="M483.17,198.49H242.75a28.86,28.86,0,0,0-28.83,28.83V367.89a28.86,28.86,0,0,0,28.83,28.83H393.27l61,61a12.08,12.08,0,0,0,20.62-8.54V396.72h8.3A28.86,28.86,0,0,0,512,367.89V227.32A28.86,28.86,0,0,0,483.17,198.49Zm4.67,169.4a4.68,4.68,0,0,1-4.67,4.67H462.79a12.07,12.07,0,0,0-12,11.15c0,.26-.05,36.29-.05,36.29l-43.85-43.86-.14-.14c-.17-.17-.33-.31-.49-.45a12,12,0,0,0-8-3H242.75a4.68,4.68,0,0,1-4.67-4.67V227.32a4.68,4.68,0,0,1,4.67-4.67H483.17a4.68,4.68,0,0,1,4.67,4.67Z" transform="translate(0 -50.76)"/>
                                    <path d="M363,285.53A12.08,12.08,0,1,0,375,297.61,12.08,12.08,0,0,0,363,285.53Z" transform="translate(0 -50.76)"/>
                                    <path d="M310.47,130.61H90.65a12.08,12.08,0,0,0,0,24.16H310.47a12.08,12.08,0,0,0,0-24.16Z" transform="translate(0 -50.76)"/>
                                    <path d="M174.07,210.46H90.65a12.08,12.08,0,0,0,0,24.16h83.43a12.08,12.08,0,1,0,0-24.16Z" transform="translate(0 -50.76)"/>
                                    <path d="M306.84,285.53a12.08,12.08,0,1,0,12.08,12.08A12.08,12.08,0,0,0,306.84,285.53Z" transform="translate(0 -50.76)"/>
                                    <path d="M419.08,285.53a12.08,12.08,0,1,0,12.08,12.08A12.08,12.08,0,0,0,419.08,285.53Z" transform="translate(0 -50.76)"/>
                                </svg>
                            </a>
                        </li>
                    </ul>
                    <div class="space"></div>
                    <ul>
                        <li>
                            <a href="https://www.instagram.com/azulanaite/" class="btn" target="_blank">
                                <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                    <title>Instagram</title>
                                    <path d="M373.41,0H138.59C62.17,0,0,62.17,0,138.59V373.41C0,449.83,62.17,512,138.59,512H373.41C449.83,512,512,449.83,512,373.41V138.59C512,62.17,449.83,0,373.41,0ZM482,373.41A108.7,108.7,0,0,1,373.41,482H138.59A108.7,108.7,0,0,1,30,373.41V138.59A108.7,108.7,0,0,1,138.59,30H373.41A108.7,108.7,0,0,1,482,138.59Z"/>
                                    <path d="M256,116c-77.2,0-140,62.8-140,140s62.8,140,140,140,140-62.8,140-140S333.2,116,256,116Zm0,250A110,110,0,1,1,366,256,110.11,110.11,0,0,1,256,366Z"/>
                                    <path d="M399.34,66.29a41.37,41.37,0,1,0,41.37,41.37A41.41,41.41,0,0,0,399.34,66.29Zm0,52.72a11.35,11.35,0,1,1,11.36-11.35A11.37,11.37,0,0,1,399.34,119Z"/>
                                </svg>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/AzulAnaite.ShaktiCacao/" class="btn" target="_blank">
                                <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 80.56 155.14">
                                    <defs>
                                        <style>.cls-1{fill:#010002;}</style>
                                    </defs>
                                    <title>Facebook</title>
                                    <path id="f" class="cls-1" d="M89.58,155.14V84.38h23.74l3.56-27.58H89.58V39.18c0-8,2.21-13.42,13.67-13.42h14.6V1.08A197.9,197.9,0,0,0,96.58,0C75.52,0,61.1,12.85,61.1,36.45V56.79H37.29V84.38H61.1v70.76Z" transform="translate(-37.29)"/>
                                </svg>
                            </a>
                        </li>
                        <li>
                            <a href="https://wa.me/525573578441" class="btn" target="_blank">
                                <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 511.51 512">
                                    <title>Whatsapp</title>
                                    <path d="M435.92,74.35A255.89,255.89,0,0,0,75.3,74.29C26.8,122.28.06,186.05,0,253.63v.13c0,40.9,10.75,82.16,31.15,119.83L.7,512l140-31.85a256.27,256.27,0,0,0,114.93,27.31h.1a255.07,255.07,0,0,0,180.44-74.3c48.54-48,75.29-111.72,75.32-179.34C511.53,186.68,484.69,122.94,435.92,74.35ZM255.74,467.5h-.09a216,216,0,0,1-102.67-26l-6.62-3.59-93.1,21.18,20.22-91.91-3.9-6.72C50.2,327,40,290.11,40,253.71,40,135.91,136.82,40,255.73,40a214.06,214.06,0,0,1,152,62.7c41.18,41,63.84,94.71,63.82,151.15C471.5,371.64,374.69,467.5,255.74,467.5Z"/>
                                    <path d="M186.15,141.86H174.94a21.52,21.52,0,0,0-15.6,7.29C154,155,138.87,169.1,138.87,197.79s21,56.41,23.89,60.3,40.47,64.64,99.93,88c49.42,19.42,59.48,15.56,70.2,14.59s34.61-14.1,39.49-27.71,4.88-25.29,3.41-27.72-5.37-3.89-11.21-6.8S330,281.18,324.68,279.23s-9.26-2.91-13.16,2.93-15.39,19.31-18.8,23.2-6.82,4.38-12.68,1.46-24.5-9.19-46.85-29.05c-17.39-15.46-29.46-35.17-32.88-41s-.36-9,2.57-11.9c2.63-2.61,6.18-6.18,9.11-9.58s3.75-5.84,5.71-9.73,1-7.3-.49-10.21-12.69-31.75-17.89-43.28h0C194.93,142.36,190.32,142,186.15,141.86Z"/>
                                </svg>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!--  -->

  <!-- Hamburger -->
  <a href="#" class="navbarHamb" data-accion="abrir">
    <div></div>
    <div></div>
    <div></div>
  </a>
  <!--  -->

<!-- Sidebar Main -->
<div class="sidebarMain">
  <div class="sidebarInner">
    <div class="sidebarContent">
      <ul>
        <li class="homeR">
          <a href="#intro" class="closeWindow"><?php echo html_entity_decode(__('home.inicio')); ?></a>
        </li>
        <li class="quienesSomosR">
          <a href="#mision" class="closeWindow"><?php echo html_entity_decode(__('home.mision')); ?></a>
        </li>
        <li class="serviciosR">
          <a href="#biografia" class="closeWindow"><?php echo html_entity_decode(__('home.biografia')); ?></a>
        </li>
        <li>
          <a href="#blog"><?php echo html_entity_decode(__('home.servicios')); ?></a>
        </li>
        <li>
          <a href="#testimonios" class="closeWindow"><?php echo html_entity_decode(__('home.testimonio')); ?></a>
        </li>
        <li class="contactoR">
          <a href="#instagram" class="closeWindow"><?php echo html_entity_decode(__('home.instagram')); ?></a>
        </li>
        <li>
          <a href="/lang/es" class="btnIdioma">Esp</a>
        </li>
        <li>
          <a href="/lang/en" class="btnIdioma">Eng</a>
        </li>
      </ul>
    </div>
    <div class="sidebarFooter">

    </div>
  </div>
</div>
<!--  -->

            <!--- INTRO --->
            <section id="intro" class="section-space">
                <div class="owl-carousel owl-theme" id="bannerCarousel">
                    <div class="item">
                        <?php echo html_entity_decode(__('home.img_01')); ?>
                        <a href="#mujer_magia" class="btn btnItem btnFormaciones show-on-large hide-on-med-and-down"><span><?php echo html_entity_decode(__('home.mas')); ?></span></a>
                        <div class="itemText show-on-large hide-on-med-and-down">
                            <?php echo html_entity_decode(__('home.txt_01')); ?>
                        </div>
                    </div>
                    <div class="item">
                        <?php echo html_entity_decode(__('home.img_02')); ?>
                        <a href="#adn" class="btn btnItem btnFormaciones show-on-large hide-on-med-and-down"><span><?php echo html_entity_decode(__('home.mas')); ?></span></a>
                        <div class="itemText show-on-large hide-on-med-and-down">
                            <?php echo html_entity_decode(__('home.txt_02')); ?>
                        </div>
                    </div>
                    <div class="item">
                        <?php echo html_entity_decode(__('home.img_03')); ?>
                        <a href="#alquimia" class="btn btnItem btnFormaciones show-on-large hide-on-med-and-down"><span><?php echo html_entity_decode(__('home.mas')); ?></span></a>
                        <div class="itemText show-on-large hide-on-med-and-down">
                            <?php echo html_entity_decode(__('home.txt_03')); ?>
                        </div>
                    </div>
                </div>

                <ul id='carousel-custom-dots' class='owl-dots'>
                  <li class='owl-dot active'><!-- Anything in here --></li>
                  <li class='owl-dot'></li>
                  <li class='owl-dot'></li>
                </ul>

                <a href="#" class="itemAnterior">
                  <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256.13 490.79">
                    <defs><style>.cls-1{fill:#f44336;}</style></defs>
                    <title>Anterior</title>
                    <path class="cls-1" d="M362.67,490.79a10.66,10.66,0,0,1-7.55-3.11L120.45,253a10.67,10.67,0,0,1,0-15.08L355.12,3.26a10.67,10.67,0,1,1,15.35,14.82l-.26.26L143.09,245.45,370.22,472.57a10.67,10.67,0,0,1-7.55,18.22Z" transform="translate(-117.33 0)"/>
                    <path d="M362.67,490.79a10.66,10.66,0,0,1-7.55-3.11L120.45,253a10.67,10.67,0,0,1,0-15.08L355.12,3.26a10.67,10.67,0,1,1,15.35,14.82l-.26.26L143.09,245.45,370.22,472.57a10.67,10.67,0,0,1-7.55,18.22Z" transform="translate(-117.33 0)"/>
                  </svg>
                </a>

                <a href="#" class="itemSiguiente">
                  <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256.15 490.8">
                    <defs><style>.cls-1{fill:#f44336;}</style></defs>
                    <title>Siguiente</title>
                    <path class="cls-1" d="M135.68,3.13A10.67,10.67,0,0,0,120.6,18.21L347.72,245.35,120.58,472.46a10.67,10.67,0,1,0,14.82,15.35l.26-.26L370.33,252.88a10.67,10.67,0,0,0,0-15.08Z" transform="translate(-117.33 0)"/>
                    <path d="M128.13,490.68a10.67,10.67,0,0,1-7.55-18.22L347.72,245.35,120.58,18.23a10.68,10.68,0,0,1,15.1-15.1L370.35,237.8a10.67,10.67,0,0,1,0,15.08L135.68,487.54A10.66,10.66,0,0,1,128.13,490.68Z" transform="translate(-117.33 0)"/>
                  </svg>
                </a>

            </section>
            <!--  -->

            <!--- MISION --->
            <section id="mision" class="section-space">
                <div class="misionHeader wow fadeInDown">
                    <?php echo html_entity_decode(__('home.mision_header')); ?>
                </div>
                <div class="row">
                    <div class="col xl3 l4 m12 s12 wow fadeInLeft">
                        <div class="misionTextLeft">
                            <?php echo html_entity_decode(__('home.mision_text')); ?>
                        </div>
                    </div>
                    <div class="col xl6 l4 m12 s12 wow fadeIn">
                        <div class="misionImg">
                            <img src="/web/home/img/mujer01_mision1.png" alt="">
                        </div>
                    </div>
                    <div class="col xl3 l4 m12 s12 wow fadeInRight">
                        <div class="misionTextRight">
                            <?php echo html_entity_decode(__('home.mision_frase')); ?>
                        </div>
                        <div class="misionBottom">
                            <button data-target="modal1" class="btn modal-trigger btnMasMision"><?php echo html_entity_decode(__('home.mas')); ?></button>
                        </div>
                    </div>
                </div>
                <div class="misionGradiante">
                    <img src="/web/home/img/mujer01_gradianteRight.png" alt="">
                </div>
            </section>
            <!--  -->

            <!-- ......... SECCIONES MAS MISION INFO -->
            @include('mision')
            <!-- ..................................... -->

            <!--- BIOGRAFIA --->
            <section id="biografia" class="section-space">
                <div class="biografiaGradianteLeft">
                    <img src="/web/home/img/mujer01_gradianteLeft.png" alt="">
                </div>
                <div class="biografiaGradianteRight">
                    <img src="/web/home/img/mujer01_gradianteRight.png" alt="">
                </div>

                <div class="row biografiaInfo">
                    <div class="col l6 m12 s12">
                        <div class="biografiaImg wow fadeInLeft">
                            <img src="/web/home/img/mujer01_biografia.png" alt="">
                        </div>
                    </div>

                    <div class="col l6 m12 s12">
                        <div class="biografiaTextRight wow fadeInRight">
                            <?php echo html_entity_decode(__('home.biografia_header')); ?>
                        </div>
                        <div class="biografiaBottom wow fadeIn" id="biografiaAccion">
                            <a href="#moreBiografia" class="btn btnMasBiografia" data-action="down"><?php echo html_entity_decode(__('home.mas')); ?></a>
                        </div>
                    </div>
                </div>
                <div class="row" style="display:none;" id="moreBiografia">
                    <div class="col xl6 l6 m12 s12">
                        <div class="biografiaInfoAdd">
                            <?php echo html_entity_decode(__('home.biografia_extra01')); ?>
                        </div>
                    </div>
                    <div class="col xl6 l6 m12 s12">
                        <div class="biografiaInfoAdd">
                            <?php echo html_entity_decode(__('home.biografia_extra02')); ?>
                        </div>
                        <div class="biografiaBottom wow fadeIn">
                            <a href="#biografia" class="btn btnMasBiografia" data-action="up"><?php echo html_entity_decode(__('home.cerrar')); ?></a>
                        </div>
                    </div>
                </div>
            </section>
            <!--  -->

            <!-- BLOG -->
            <section id="blog" class="section-space">
                <div class="row">
                    <div class="col l6 m12 s12 blogCols">
                        <div class="blogInfo">
                            <a href="#mujer_magia" class="servicioLink btnFormaciones">
                                <div class="blogCover wow fadeInDown">
                                    <img src="/web/home/img/mujer_magia_blog_001.jpg?var=<?php echo rand() ?>" alt="" srcset="">
                                    <img class="coverMain" src="/web/home/img/mujer_magia_blog_01.jpg?var=<?php echo rand() ?>" alt="" srcset="">
                                    <?php echo html_entity_decode(__('home.blog_mujer_magia_portada')); ?>
                                </div>
                            </a>
                            <div class="blogText wow fadeInUp">
                                <?php echo html_entity_decode(__('home.blog_mujer_magia')); ?>
                                <a href="#mujer_magia" class="btnFormaciones"><?php echo html_entity_decode(__('home.leer_mas')); ?></a>
                            </div>

                        </div>
                    </div>
                    <div class="col l6 m12 s12 blogCols">
                        <div class="blogInfo">
                            <a href="#shakti_cacao" class="servicioLink btnFormaciones">
                                <div class="blogCover wow fadeInDown">
                                    <img src="/web/home/img/mujer_magia_blog_002.jpg?var=<?php echo rand() ?>" alt="" srcset="">
                                    <img class="coverMain" src="/web/home/img/mujer_magia_blog_02.jpg?var=<?php echo rand() ?>" alt="" srcset="">
                                    <img class="logoMain" src="/web/home/img/mujer_magia_blog_shakti.png" alt="" srcset="">
                                </div>
                            </a>
                            <div class="blogText wow fadeInUp">
                                <?php echo html_entity_decode(__('home.blog_shakti')); ?>
                                <a href="#shakti_cacao" class="btnFormaciones"><?php echo html_entity_decode(__('home.leer_mas')); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col l6 m12 s12 blogCols">
                        <div class="blogInfo">
                           <a href="#alquimia" class="servicioLink btnFormaciones">
                                <div class="blogCover wow fadeInDown">
                                    <img src="/web/home/img/mujer_magia_blog_003.jpg?var=<?php echo rand() ?>" alt="" srcset="">
                                    <img class="coverMain" src="/web/home/img/mujer_magia_blog_03.jpg?var=<?php echo rand() ?>" alt="" srcset="">
                                    <?php echo html_entity_decode(__('home.blog_cacao_portada')); ?>
                                </div>
                            </a>
                            <div class="blogText wow fadeInUp">
                                <?php echo html_entity_decode(__('home.blog_cacao')); ?>
                                <a href="#alquimia" class="btnFormaciones"><?php echo html_entity_decode(__('home.leer_mas')); ?></a>
                            </div>

                        </div>
                    </div>
                    <div class="col l6 m12 s12 blogCols">
                        <div class="blogInfo">
                            <a href="#adn" class="servicioLink btnFormaciones">
                                <div class="blogCover wow fadeInDown">
                                    <img src="/web/home/img/mujer_magia_blog_04.jpg?var=<?php echo rand() ?>" alt="" srcset="">
                                    <img class="coverMain" src="/web/home/img/mujer_magia_blog_004.jpg?var=<?php echo rand() ?>" alt="" srcset="">
                                </div>
                            </a>
                            <div class="wow fadeInUp">
                                <div class="blogADN">
                                    <?php echo html_entity_decode(__('home.blog_adn_title')); ?>
                                </div>
                                <div class="blogText blogText01">
                                    <?php echo html_entity_decode(__('home.blog_adn')); ?>
                                    <a href="#adn" class="btnFormaciones"><?php echo html_entity_decode(__('home.leer_mas')); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--  -->

            <!-- ......... SECCCIONES INDIVIDUALES FORMACIONES -->
            @include('formaciones')
            <!-- ..................................... -->

            <!-- TESTIMONIOS -->
            <section id="testimonios" class="section-space">
                <div class="testimoniosContainer">
                    <div class="testimoniosHeader wow fadeInDown">
                        <?php echo html_entity_decode(__('home.testimonio_header')); ?>
                    </div>
                    <div class="owl-carousel owl-theme" id="testimoniosCarousel">
                        <div class="item">
                            <div class="itemImg">
                                <img src="/web/home/img/mujer_magia_constanza.png">
                            </div>
                            <div class="itemNombre">
                                <h2>Contanza Lechuga</h2>
                            </div>
                            <?php echo html_entity_decode(__('home.test_01')); ?>
                        </div>
                        <div class="item">
                            <div class="itemImg">
                                <img src="/web/home/img/mujer_magia_anais.png">
                            </div>
                            <div class="itemNombre">
                                <h2>Anais Ceballos</h2>
                            </div>
                            <?php echo html_entity_decode(__('home.test_02')); ?>
                        </div>
                        <div class="item">
                            <div class="itemImg">
                                <img src="/web/home/img/mujer_magia_mercedes.png">
                            </div>
                            <div class="itemNombre">
                                <h2>Mercedes</h2>
                            </div>
                            <?php echo html_entity_decode(__('home.test_03')); ?>
                        </div>
                        <div class="item">
                            <div class="itemImg">
                                <img src="/web/home/img/mujer_magia_isabel.png">
                            </div>
                            <div class="itemNombre">
                                <h2>Ana Brenda</h2>
                            </div>
                            <?php echo html_entity_decode(__('home.test_04')); ?>
                        </div>
                        <div class="item">
                            <div class="itemImg">
                                <img src="/web/home/img/mujer_magia_ana_brenda.png">
                            </div>
                            <div class="itemNombre">
                                <h2>Ana Fer Islas</h2>
                            </div>
                            <?php echo html_entity_decode(__('home.test_05')); ?>
                        </div>
                        <div class="item">
                            <div class="itemImg">
                                <img src="/web/home/img/mujer_magia_ana_fer.png">
                            </div>
                            <div class="itemNombre">
                                <h2>Isabel</h2>
                            </div>
                            <?php echo html_entity_decode(__('home.test_06')); ?>
                        </div>
                    </div>
                </div>

                <a href="#" class="itemAnterior">
                  <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256.13 490.79">
                    <defs><style>.cls-1{fill:#f44336;}</style></defs>
                    <title>Anterior</title>
                    <path class="cls-1" d="M362.67,490.79a10.66,10.66,0,0,1-7.55-3.11L120.45,253a10.67,10.67,0,0,1,0-15.08L355.12,3.26a10.67,10.67,0,1,1,15.35,14.82l-.26.26L143.09,245.45,370.22,472.57a10.67,10.67,0,0,1-7.55,18.22Z" transform="translate(-117.33 0)"/>
                    <path d="M362.67,490.79a10.66,10.66,0,0,1-7.55-3.11L120.45,253a10.67,10.67,0,0,1,0-15.08L355.12,3.26a10.67,10.67,0,1,1,15.35,14.82l-.26.26L143.09,245.45,370.22,472.57a10.67,10.67,0,0,1-7.55,18.22Z" transform="translate(-117.33 0)"/>
                  </svg>
                </a>

                <a href="#" class="itemSiguiente">
                  <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256.15 490.8">
                    <defs><style>.cls-1{fill:#f44336;}</style></defs>
                    <title>Siguiente</title>
                    <path class="cls-1" d="M135.68,3.13A10.67,10.67,0,0,0,120.6,18.21L347.72,245.35,120.58,472.46a10.67,10.67,0,1,0,14.82,15.35l.26-.26L370.33,252.88a10.67,10.67,0,0,0,0-15.08Z" transform="translate(-117.33 0)"/>
                    <path d="M128.13,490.68a10.67,10.67,0,0,1-7.55-18.22L347.72,245.35,120.58,18.23a10.68,10.68,0,0,1,15.1-15.1L370.35,237.8a10.67,10.67,0,0,1,0,15.08L135.68,487.54A10.66,10.66,0,0,1,128.13,490.68Z" transform="translate(-117.33 0)"/>
                  </svg>
                </a>
            </section>
            <!--  -->

            <!-- ......... SECCCIONES INDIVIDUALES FORMACIONES -->
            @include('testimonios')
            <!-- ..................................... -->

            <!-- INSTAGRAM -->
            <section id="instagram" class="section-space">
                <div class="row">
                    <div class="col xl4 l4 m12 s12">
                        <div class="instagramHeader wow fadeInDown">
                            <?php echo html_entity_decode(__('home.instagramHeader')); ?>
                        </div>
                    </div>
                    <div class="col xl8 l8 m12 s12" >
                        <div class="instagramContainer">
                            <div id="instafeed" class="owl-carousel owl-theme"></div>

                <a href="#" class="itemAnterior">
                  <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256.13 490.79">
                    <defs><style>.cls-1{fill:#f44336;}</style></defs>
                    <title>Anterior</title>
                    <path class="cls-1" d="M362.67,490.79a10.66,10.66,0,0,1-7.55-3.11L120.45,253a10.67,10.67,0,0,1,0-15.08L355.12,3.26a10.67,10.67,0,1,1,15.35,14.82l-.26.26L143.09,245.45,370.22,472.57a10.67,10.67,0,0,1-7.55,18.22Z" transform="translate(-117.33 0)"/>
                    <path d="M362.67,490.79a10.66,10.66,0,0,1-7.55-3.11L120.45,253a10.67,10.67,0,0,1,0-15.08L355.12,3.26a10.67,10.67,0,1,1,15.35,14.82l-.26.26L143.09,245.45,370.22,472.57a10.67,10.67,0,0,1-7.55,18.22Z" transform="translate(-117.33 0)"/>
                  </svg>
                </a>

                <a href="#" class="itemSiguiente">
                  <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256.15 490.8">
                    <defs><style>.cls-1{fill:#f44336;}</style></defs>
                    <title>Siguiente</title>
                    <path class="cls-1" d="M135.68,3.13A10.67,10.67,0,0,0,120.6,18.21L347.72,245.35,120.58,472.46a10.67,10.67,0,1,0,14.82,15.35l.26-.26L370.33,252.88a10.67,10.67,0,0,0,0-15.08Z" transform="translate(-117.33 0)"/>
                    <path d="M128.13,490.68a10.67,10.67,0,0,1-7.55-18.22L347.72,245.35,120.58,18.23a10.68,10.68,0,0,1,15.1-15.1L370.35,237.8a10.67,10.67,0,0,1,0,15.08L135.68,487.54A10.66,10.66,0,0,1,128.13,490.68Z" transform="translate(-117.33 0)"/>
                  </svg>
                </a>
                        </div>
                    </div>
                </div>
                <div class="space"></div>
                <div class="row">
                    <div class="col xl12 l12 m12 s12 show-on-medium-and-down hide-on-large-only">
                        <div class="formHeader wow fadeInDown">
                            <?php echo html_entity_decode(__('home.formHeader')); ?>
                        </div>
                    </div>
                    <div class="col xl12 l12 m12 s12">
                        <div class="formContainer">
                            <form action="" method="" id="contactoformulario" autocomplete="off">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col xl4 l4 m12 s12 show-on-large hide-on-med-and-down">
                                        <div class="formHeader">
                                            <?php echo html_entity_decode(__('home.formHeader')); ?>
                                        </div>
                                    </div>
                                    <div class="col xl4 l4 m12 s12">
                                        <input class="inputFirst" type="text" name="nombre" placeholder="<?php echo html_entity_decode(__('home.nombre_field')); ?>">
                                    </div>
                                    <div class="col xl4 l4 m12 s12">
                                        <input class="inputFirst" type="text" name="telefono" placeholder="<?php echo html_entity_decode(__('home.celular_field')); ?>">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col xl4 l4 m12 s12">
                                        <input type="email" name="correo" placeholder="<?php echo html_entity_decode(__('home.correo_field')); ?>">
                                    </div>
                                    <div class="col xl5 l5 m12 s12">
                                        <input type="text" name="mensaje" placeholder="<?php echo html_entity_decode(__('home.mensaje_field')); ?>">
                                    </div>
                                    <div class="col xl3 l3 m12 s12 center-align">
                                        <button type="submit" class="btn btnContacto"><?php echo html_entity_decode(__('home.enviar_boton')); ?></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
            <!--  -->

            <!-- FOOTER -->
            <footer class="page-footer section-space" id="footer">
                <div class="footerContent">
                    <div class="row">
                        <div class="col xl4 l4 m12 s12 center-align show-on-large hide-on-med-and-down">
                            <?php echo html_entity_decode(__('home.copyright')); ?>
                        </div>
                        <div class="col xl4 l4 m12 s12 center-align">
                            <a href="/">
                                <img class="img-responsive" src="/web/logo_azul_footer.png">
                            </a>
                        </div>
                        <div class="col xl4 l4 m12 s12 center-align">
                            <ul>
                                <li>
                                    <a href="https://www.instagram.com/azulanaite/" class="btn" target="_blank">
                                        <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                            <title>Instagram</title>
                                            <path d="M373.41,0H138.59C62.17,0,0,62.17,0,138.59V373.41C0,449.83,62.17,512,138.59,512H373.41C449.83,512,512,449.83,512,373.41V138.59C512,62.17,449.83,0,373.41,0ZM482,373.41A108.7,108.7,0,0,1,373.41,482H138.59A108.7,108.7,0,0,1,30,373.41V138.59A108.7,108.7,0,0,1,138.59,30H373.41A108.7,108.7,0,0,1,482,138.59Z"/>
                                            <path d="M256,116c-77.2,0-140,62.8-140,140s62.8,140,140,140,140-62.8,140-140S333.2,116,256,116Zm0,250A110,110,0,1,1,366,256,110.11,110.11,0,0,1,256,366Z"/>
                                            <path d="M399.34,66.29a41.37,41.37,0,1,0,41.37,41.37A41.41,41.41,0,0,0,399.34,66.29Zm0,52.72a11.35,11.35,0,1,1,11.36-11.35A11.37,11.37,0,0,1,399.34,119Z"/>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.facebook.com/AzulAnaite.ShaktiCacao/" class="btn" target="_blank">
                                        <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 80.56 155.14">
                                            <defs>
                                                <style>.cls-1{fill:#010002;}</style>
                                            </defs>
                                            <title>Facebook</title>
                                            <path id="f" class="cls-1" d="M89.58,155.14V84.38h23.74l3.56-27.58H89.58V39.18c0-8,2.21-13.42,13.67-13.42h14.6V1.08A197.9,197.9,0,0,0,96.58,0C75.52,0,61.1,12.85,61.1,36.45V56.79H37.29V84.38H61.1v70.76Z" transform="translate(-37.29)"/>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://wa.me/525573578441" class="btn" target="_blank">
                                        <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 511.51 512">
                                            <title>Whatsapp</title>
                                            <path d="M435.92,74.35A255.89,255.89,0,0,0,75.3,74.29C26.8,122.28.06,186.05,0,253.63v.13c0,40.9,10.75,82.16,31.15,119.83L.7,512l140-31.85a256.27,256.27,0,0,0,114.93,27.31h.1a255.07,255.07,0,0,0,180.44-74.3c48.54-48,75.29-111.72,75.32-179.34C511.53,186.68,484.69,122.94,435.92,74.35ZM255.74,467.5h-.09a216,216,0,0,1-102.67-26l-6.62-3.59-93.1,21.18,20.22-91.91-3.9-6.72C50.2,327,40,290.11,40,253.71,40,135.91,136.82,40,255.73,40a214.06,214.06,0,0,1,152,62.7c41.18,41,63.84,94.71,63.82,151.15C471.5,371.64,374.69,467.5,255.74,467.5Z"/>
                                            <path d="M186.15,141.86H174.94a21.52,21.52,0,0,0-15.6,7.29C154,155,138.87,169.1,138.87,197.79s21,56.41,23.89,60.3,40.47,64.64,99.93,88c49.42,19.42,59.48,15.56,70.2,14.59s34.61-14.1,39.49-27.71,4.88-25.29,3.41-27.72-5.37-3.89-11.21-6.8S330,281.18,324.68,279.23s-9.26-2.91-13.16,2.93-15.39,19.31-18.8,23.2-6.82,4.38-12.68,1.46-24.5-9.19-46.85-29.05c-17.39-15.46-29.46-35.17-32.88-41s-.36-9,2.57-11.9c2.63-2.61,6.18-6.18,9.11-9.58s3.75-5.84,5.71-9.73,1-7.3-.49-10.21-12.69-31.75-17.89-43.28h0C194.93,142.36,190.32,142,186.15,141.86Z"/>
                                        </svg>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col xl4 l4 m12 s12 center-align show-on-medium-and-down hide-on-large-only">
                            <?php echo html_entity_decode(__('home.copyright')); ?>
                        </div>
                    </div>
                </div>
                <div class="footer-copyright"></div>
            </footer>
            <!--  -->

        <!-- SCRIPTS -->
        <script type="text/javascript">
            var idioma = "{{Session::get('lang')}}";
            console.log(idioma);
        </script>
        <script type="text/javascript" src="/js/jquery.min.js"></script>
        <script type="text/javascript" src="/web/navbar/script.min.js?var=<?php echo rand() ?>"></script>
        <script type="text/javascript" src="/js/jquery.easing.js"></script>
        <script type="text/javascript" src="/js/materialize.min.js"></script>
        <script type="text/javascript" src="/js/sweetalert2.min.js"></script>
        <script type="text/javascript" src="/js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="/js/additional-methods.min.js"></script>
        <script type="text/javascript" src="/js/wow.min.js"></script>
        <script type="text/javascript" src="/js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="/js/noframework.waypoints.min.js"></script>
        <script type="text/javascript" src="/js/instafeed.min.js"></script>
        <script src="/js/jquery.waypoints.min.js"></script>

        <script src="https://ig.instant-tokens.com/users/ff3d6a15-b88c-4dd0-9cab-d76ca213b9a1/instagram/17841401099593304/token.js?userSecret=m4twuxjphtz9lwqpxmq9"></script>
        <script type="text/javascript" src="/web/home/scripts.min.js?var=<?php echo rand() ?>"></script>

        <script type="text/javascript">
            var owlSlideSpeed = 300;
            new WOW().init();
            $(document).ready(function(){
                $('.modal').modal({
                    dismissible: true, // Modal can be dismissed by clicking outside of the modal
                    opacity: 0, // Opacity of modal background
                    inDuration: 300, // Transition in duration
                    outDuration: 200, // Transition out duration
                    startingTop: '4%', // Starting top style attribute
                    endingTop: '10%', // Ending top style attribute
                    ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                        console.log('Open Modal');
                    },
                    complete: function() {
                        console.log('Close Modal')
                    } // Callback for Modal close
                });

                var banner      = $('#bannerCarousel');
                var testimonios = $('#testimoniosCarousel');

                banner.owlCarousel({
                  animateIn: 'fadeIn',
                  animateOut: 'fadeOut',
                  loop: true,
                  margin: 0,
                  dotsContainer: '#carousel-custom-dots',
                  items: 1,
                  responsiveClass: true,
                  autoplay: false,
                  autoplayTimeout:6000,
                  autoplayHoverPause:false
                });

                testimonios.owlCarousel({
                  animateIn: 'fadeIn',
                  animateOut: 'fadeOut',
                  loop: false,
                  margin: 0,
                  dots: false,
                  mouseDrag: false,
                  touchDrag:false,
                  pullDrag: false,
                  responsiveClass: true,
                  autoplay: false,
                  autoplayTimeout:6000,
                  autoplayHoverPause:false,
                  responsive : {
                      // breakpoint from 0 up
                      0 : {
                          items: 1
                      },
                      // breakpoint from 480 up
                      480 : {
                          items: 1
                      },
                      // breakpoint from 768 up
                      768 : {
                          items: 3
                      }
                  }
                });

                $("#intro .itemAnterior").click(function (e) {
                    e.preventDefault();
                    banner.trigger("prev.owl.carousel", [owlSlideSpeed]);
                });

                $("#intro .itemSiguiente").click(function (e) {
                    e.preventDefault();
                    banner.trigger("next.owl.carousel", [owlSlideSpeed]);
                });

                $("#testimonios .itemAnterior").click(function (e) {
                    e.preventDefault();
                    testimonios.trigger("prev.owl.carousel", [owlSlideSpeed]);
                });

                $("#testimonios .itemSiguiente").click(function (e) {
                    e.preventDefault();
                    testimonios.trigger("next.owl.carousel", [owlSlideSpeed]);
                });
            });
        </script>
        <!--  -->
    </body>
</html>
