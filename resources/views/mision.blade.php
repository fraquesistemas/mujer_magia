<div id="modal1" class="modal">
 <a href="#!" class="modal-close btnCloseMisionMore">X</a>
  <div class="modal-content">
    <div class="row">
        <div class="col xl6 l6 m12 s12">
            <?php echo html_entity_decode(__('home.mision_mas_01')); ?>
        </div>
        <div class="col xl6 l6 m12 s12">
            <?php echo html_entity_decode(__('home.mision_mas_02')); ?>
        </div>
    </div>
  </div>
  <div class="modal-footer"></div>
</div>
