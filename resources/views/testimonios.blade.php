<!--  CONSTANZA LECHUGA -->
<section id="constanza_lechuga" style="display:none;">
    <div class="row">
        <div class="col xl6 l6 m12 12 center-align">
            <img class="testimonioInfoAdd-img" src="/web/home/img/mujer_magia_constanza.png">
        </div>
        <div class="col xl6 l6 m12 12">
            <div class="testimonioInfoAdd">
                <?php echo html_entity_decode(__('home.test__01')); ?>
            </div>
        </div>
    </div>
</section>
<!--  -->

 <!--  ANAIS CEBALLOS -->
 <section id="anais_ceballos" style="display:none;">
     <div class="row">
         <div class="col xl6 l6 m12 12 center-align">
             <img class="testimonioInfoAdd-img" src="/web/home/img/mujer_magia_anais.png">
         </div>
         <div class="col xl6 l6 m12 12">
             <div class="testimonioInfoAdd">
                  <?php echo html_entity_decode(__('home.test__02')); ?>
             </div>
         </div>
     </div>
</section>
<!--  -->

<!-- MERCEDES -->
<section id="mercedes" style="display:none;">
    <div class="row">
        <div class="col xl6 l6 m12 12 center-align">
            <img class="testimonioInfoAdd-img" src="/web/home/img/mujer_magia_mercedes.png">
        </div>
        <div class="col xl6 l6 m12 12">
            <div class="testimonioInfoAdd">
                <?php echo html_entity_decode(__('home.test__03')); ?>
            </div>
        </div>
    </div>
</section>
<!--  -->

<!-- ANA BRENDA -->
<section id="ana_brenda"  style="display:none;">
    <div class="row">
        <div class="col xl6 l6 m12 12 center-align">
            <img class="testimonioInfoAdd-img" src="/web/home/img/mujer_magia_isabel.png">
        </div>
        <div class="col xl6 l6 m12 12">
            <div class="testimonioInfoAdd">
                <?php echo html_entity_decode(__('home.test__04')); ?>
            </div>
        </div>
    </div>
</section>
<!--  -->

<!-- ANA FER ISLAS -->
<section id="ana_fer_islas" style="display:none;">
    <div class="row">
        <div class="col xl6 l6 m12 12 center-align">
            <img class="testimonioInfoAdd-img" src="/web/home/img/mujer_magia_ana_brenda.png">
        </div>
        <div class="col xl6 l6 m12 12">
            <div class="testimonioInfoAdd">
                <?php echo html_entity_decode(__('home.test__05')); ?>
            </div>
        </div>
    </div>
</section>
<!--  -->

<!-- ISABEL -->
<section id="isabel" style="display:none;">
    <div class="row">
        <div class="col xl6 l6 m12 12 center-align">
            <img class="testimonioInfoAdd-img" src="/web/home/img/mujer_magia_ana_fer.png">
        </div>
        <div class="col xl6 l6 m12 12">
            <div class="testimonioInfoAdd">
                <?php echo html_entity_decode(__('home.test__06')); ?>
            </div>
        </div>
    </div>
</section>
<!--  -->
