<!--  ADN -->
<section id="adn" style="display:none;">
    <div class="row">
        <div class="col l8 m12 s12">
            <div class="adnImgsLeft">
                <?php echo html_entity_decode(__('home.adn_portada')); ?>
            </div>
        </div>
        <div class="col l4 m12 s12">
            <div class="adnImgsRight">
                <?php echo html_entity_decode(__('home.adn_header')); ?>
            </div>
        </div>
    </div>
    <div class="row">
    <div class="col l4 m12 s12 ">
            <div class="adnContent">
                <?php echo html_entity_decode(__('home.adn_info_01')); ?>
            </div>
        </div>
        <div class="col l4 m12 s12 ">
            <div class="adnContent">
                <?php echo html_entity_decode(__('home.adn_info_02')); ?>
            </div>
            <div class="adnBottom show-on-large hide-on-med-and-down">
                <a href="#instagram" class="btn btnContacto"><?php echo html_entity_decode(__('home.contacto')); ?></a>
            </div>
        </div>
        <div class="col l4 m12 s12 show-on-large hide-on-med-and-down">
            <div class="adnContentImg">
                <img src="/web/home/img/adn03.png?var=<?php echo rand() ?>" alt="">
            </div>
        </div>
        <div class="col xl12 l12 m12 s12 show-on-medium-and-down hide-on-large-only">
            <div class="adnBottom">
                <a href="#instagram" class="btn btnContacto"><?php echo html_entity_decode(__('home.contacto')); ?></a>
            </div>
        </div>
    </div>
</section>
<!--  -->

 <!--  ALQUIMIA -->
 <section id="alquimia" style="display:none;">
    <div class="row">
        <div class="col l8 m12 s12">
            <div class="alquimiaImgsLeft">
                <?php echo html_entity_decode(__('home.alquimia_portada')); ?>
            </div>
        </div>
        <div class="col l4 m12 s12 show-on-large hide-on-med-and-down">
            <div class="alquimiaImgsRight">
                <img src="/web/home/img/alquimia02.png?var=<?php echo rand() ?>" alt="">
            </div>
        </div>
    </div>
    <div class="row">
    <div class="col l4 m12 s12 ">
            <div class="alquimiaContent">
                <?php echo html_entity_decode(__('home.alquimia_info_01')); ?>
            </div>
        </div>
        <div class="col l4 m12 s12 ">
            <div class="alquimiaContent">
                <?php echo html_entity_decode(__('home.alquimia_info_02')); ?>
            </div>
            <div class="alquimiaBottom show-on-large hide-on-med-and-down">
                <a href="https://alquimiaycacao.com/" target="_blank" class="btn btnContacto"><?php echo html_entity_decode(__('home.mas')); ?></a>
            </div>
        </div>
        <div class="col l4 m12 s12 show-on-large hide-on-med-and-down">
            <div class="alquimiaContentImg">
                <img src="/web/home/img/alquimia03.png?var=<?php echo rand() ?>" alt="">
            </div>
        </div>
        <div class="col xl12 l12 m12 s12 show-on-medium-and-down hide-on-large-only">
            <div class="alquimiaBottom">
                <a href="https://alquimiaycacao.com/" target="_blank" class="btn btnContacto"><?php echo html_entity_decode(__('home.mas')); ?></a>
            </div>
        </div>
    </div>
</section>
<!--  -->

<!-- MUJER MAGIA -->
<section id="mujer_magia" style="display:none;">
    <div class="row">
        <div class="col xl8 l8 m12 s12">
            <div class="mujer_magiaImg">
                <?php echo html_entity_decode(__('home.mujer_magia_portada')); ?>
            </div>
            <div class="col xl6 l6 m12 s12">
                <div class="mujerMagiaInfo">
                    <?php echo html_entity_decode(__('home.mujer_magia_info_01')); ?>
                </div>
            </div>
            <div class="col xl6 l6 m12 s12">
                <div class="mujerMagiaInfo">
                    <?php echo html_entity_decode(__('home.mujer_magia_info_02')); ?>
                </div>
            </div>
        </div>
        <div class="col xl4 l4 m12 s12">
            <div class="mujerMagiaInfo">
                <?php echo html_entity_decode(__('home.mujer_magia_info_03')); ?>
            </div>
            <div class="mujerMagiFooter">
                <a href="http://mujermagia.com/" target="_blank" class="btn btnMas"><?php echo html_entity_decode(__('home.mas')); ?></a>
            </div>
        </div>
    </div>
</section>
<!--  -->

<!-- SHAKTI CACAO -->
<section id="shakti_cacao" style="display:none;">
    <div class="row">
        <div class="col xl8 l8 m12 s12">
            <div class="shakti_cacaoImg">
                <img src="/web/home/img/hackti_cacao_portada.png?var=<?php echo rand() ?>">
            </div>
            <div class="col xl6 l6 m12 s12">
                <div class="shaktiCacaoInfo">
                    <?php echo html_entity_decode(__('home.shakti_cacao_info_01')); ?>
                </div>
            </div>
            <div class="col xl6 l6 m12 s12">
                <div class="shaktiCacaoInfo">
                    <?php echo html_entity_decode(__('home.shakti_cacao_info_02')); ?>
                </div>
                <div class="shaktiFooter show-on-large hide-on-med-and-down">
                    <a href="#instagram" class="btn btnMas"><?php echo html_entity_decode(__('home.shakti_contact')); ?></a>
                </div>
            </div>
        </div>
        <div class="col xl4 l4 m12 s12">
            <div class="shakti_cacaoImg">
                <img src="/web/home/img/mujer_shakit_cacao_01.jpg?var=<?php echo rand() ?>">
            </div>
            <div class="shaktiCacaoInfo">
                <?php echo html_entity_decode(__('home.shakti_cacao_info_03')); ?>
            </div>
            <div class="shakti_cacaoImg">
                <img src="/web/home/img/mujer_shakit_cacao_02.jpg?var=<?php echo rand() ?>">
            </div>
            <div class="shaktiFooter show-on-medium-and-down hide-on-large-only">
                <a href="#instagram" class="btn btnMas"><?php echo html_entity_decode(__('home.shakti_contact')); ?></a>
            </div>
        </div>
    </div>
</section>
<!--  -->
