<?php
$numeros = rand();

return [

    /*
    |--------------------------------------------------------------------------
    | PAGINA INGLES INICIO
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    // OPCIONES
    'inicio'    => 'Home',
    'mision'    => 'Mission',
    'biografia' => 'Biography',
    'servicios' => 'Training',
    'testimonio'=> 'Testimonials',
    'instagram' => 'Instagram',
    // ---------------

    // FOOTER
    'copyright' => '<p>All rights reserved<br>Design: Vive_Magenta<br>Developed by: <a href="https://fraquesistemas.com.mx" target="_blank">Fraque Sistemas</a></p>',
    // -----------------

    'leer_mas'  => 'Read More',
    'mas'       => 'More',
    'cerrar'    => 'Close',
    'contacto'  => 'Suscribe',

    // BANNERS
    'img_01'   => '<img src="/web/home/img/galeria/mujer_magia_galeria_01_en.jpg?var='.$numeros.'">',
    'img_02'   => '<img src="/web/home/img/galeria/mujer_magia_galeria_02_en.jpg?var='.$numeros.'">',
    'img_03'   => '<img src="/web/home/img/galeria/mujer_magia_galeria_03_en.jpg?var='.$numeros.'">',

    'txt_01'   => '<h2>Ready To Get Your Magic Back?</h2>
                            <p>
                                Awaken your gifts, discover the hidden treasures that are in your story.
                            </p>',

    'txt_02'   => '<h2>I have a gift to share.</h2>
                            <p>A powerful tool that opens, awakens and activates your heart, the nucleus of each cell and the DNA, in order to live your life fully.</p>',

    'txt_03'   => '<h2>It is an invitation to start a journey.</h2>
                            <p>A journey of deep exploration within ourselves to find the wounds that hide, within each one of us, the gifts that we came to awaken in this life.</p>',
    // -------------------

    // MISION
    'mision_header' => '<h2>Mission</h2>',
    'mision_text'   => '<p>Azul Anaite is a ceremonialist woman and ritualist, guardian of the sacred feminine. She holds workshops, circles and trainings for women who <span>wish to awaken their magic, to find the gifts that are hidden in their history, as well as remembering the path of taking nature as a life guide.</span></p>',
    'mision_frase'  => '<h2>“If you love something enough, that something <span>will show you all its secrets”</span></h2>',

    'mision_mas_01' => '<div class="misionTextMore">
        <p>She, along with her husband, are the creators of <span class="nameCacao">Cacao Ancestral</span>, a project that was born from their encounter with cacao in 2014. This sacred seed opened for them a path with heart and which accompanies them in their lives as part of their destiny. They are both in a path of constant exploration, accompanied by their teachers Keith Wilson and Scott Harshman.</p>
        <p>Along with her husband, she holds a space of training and initiation for those who wish to to awaken the ceremonialist within and share the medicine that exists within themselves.She also holds retreats where the intention is <span>to weave a new humanity, a place where magic, trust and a place where the relationship between nature and humankind starts to awaken.</span></p>
                </div>',

    'mision_mas_02' => '<div class="misionTextMore misionTextMore01">
        <p>Anaite is mother of 4 children, three from her former marriage. Family hasbecome her biggest school, walking as a couple is her constant training, theshared vision of becoming elders for those to come and guardians of their family, is their path.</p>
        <p>They desire To awaken the listening of the deep wisdom of the earth, to discover the gifts and virtues within each human being and in all humanity, woven together with all the connections that exist in the universe.</p>
        <p>They long to discover the history that each land keeps, to listen these stories directly from nature,<span>to remember that we all have a direct connection with divinity, and that our path is our most precious and sacred gift.</span></p>
    </div>',
    // ---------------------------

    // BIOGRAFIA
    'biografia_header'  => '<img class="biografiaTitle" src="/web/home/img/mujer01_biografiaTitle_en.png?var='.$numeros.'" alt="">
    <br>
    <p>Was born in Guatemala and grew up in Mexico. Her parents seeded in her the love of justice, culture, ancestral memory and life. During her youth she was in constant contact with elders form different ethnicities.<span>With Keith Wilson, her teacher, she started walking the path of cacao, the path back to the heart. With her teacher Scott Harshman she received the teaching of the medicine wheel and the astrology of the earth from the Ojibwe linage.</span></p>
    <p>She gave birth to 4 children; and with them she awakened as midwife of wisdom.</p>
    <h2>She has created various transformative reconnection and awakening processes of, where she accompanies others to awaken their magic by sharing the medicine that inhabits each one of them.</h2>',

    'biografia_extra01'   => '<p>She is the creator of the technique “Activacion del Corazón” (Heart Activation),the process of femenine gestation . “Enciende tu sabiduría femenina; Shake Cacao” (Ignite you feminine wisdom; Shakti Cacao) , “ El Camino De La Diosa” (The Path Of The Goddess), “Mujer Magic;Tu Encuentro Con Lo Sagrado Femenino” ( Magic woman ; Your Encounter With The Sacred Feminine),together with her husband they share the training “Alquimia y Cacao; El Camino Del Corazón” (Alchemy and Cacao; The Path Of The Heart”..</p>
    <pShe shares trainings for women and for lightworkers and she organizes journeys to the heart of Lake Atitlan where they can encounter their destiny.</p>
    <p>She has dedicated herself to tracing a path back to the body after witnessing the memory of trauma in humanity.</p>',

    'biografia_extra02'   => '<h2>She has accompanied hundreds of women in the process of giving birth to themselves, being born as magicians, healers and guardians of their own medicine.</h2>
    <p>There is much more than you imagine in this world and within ourselves. My mission is to open new paths, new ways to live life.</p>',

    // ---------------------------

    // BLOG
    'blog_mujer_magia_portada'  => '<img class="logoMain" src="/web/home/img/mujer_magia_blog_en.png?var='.$numeros.'" alt="" srcset="">',
    'blog_mujer_magia'  => '<p>It was a call to woman magicians, for the who are ready to reclaim their magic. Mujer Magia is a path to find the power that awakens within you.</p>
    <h3 class="blogSubtitle01">Are you ready to love and trust yourself?</h3>',

    'blog_shakti'  => '<p>Be reborn, give birth to yourself in consciousness, awaken your capacity to heal and replenish yourself and discover yourself as co-creator of your reality.</p>
    <h3 class="blogSubtitle02">Come in deep contact with what it means to be a woman and recovering your feminine magic.</h3>',

    'blog_cacao_portada'  => '<img class="logoMain" src="/web/home/img/mujer_magia_blog_cacao_en.png?var='.$numeros.'" alt="" srcset="">',
    'blog_cacao'  => '<p>The intention of this training is to accompany you in awakening your inner resources so you can open your eyes to the deep truth; You are much more than what you think, and the world needs your medicine.</p>
    <h3 class="blogSubtitle01 colorBrown">¡Thanks you for listening to this calling!</h3>',

    'blog_adn_title'    => '<img src="/web/home/img/mujer_magia_ADN_en.png?var='.$numeros.'" alt="" srcset="">',
    'blog_adn'     => '<p>I was given the Divine Nature Activation as a gift for humanity, a powerful tool that opens, awakens and activates your heart, the core of each cell and DNA so you can live a fulfilled life.</p>
    <h3 class="blogSubtitle02 colorBeige">¡Luminous blessings to each heart that awakens!</h3>',
    // -----------

    // TESTIMONIOS
    'testimonio_header'     => '<h2>Testimonials</h2>',
    'test_01'   => '                            <div class="itemFrase">
                                <h2 class="blueText">... by naming and embracing my darkness, my light inside looks out and light up all the treasures.</h2>
                            </div>
                            <div class="itemText">
                                <p>
                                    The accompaniment that Azul has given me during this inward journey has been a gift full of magic.
                                </p>
                                <a href="#constanza_lechuga" class="sectionPerson">Read More</a>
                            </div>',
    'test_02'   => '<div class="itemFrase">
                                <h2 class="goldenText">That we are all portal between the sky and the earth,that everything we need we just have to ask</h2>
                            </div>
                            <div class="itemText">
                                <p>Dear Anaité, who taught me from your loving example: That Being a Woman is not at odds with being a mother, wife, lover and manifesting the dreams of the heart.</p>
                                <a href="#anais_ceballos" class="sectionPerson">Read More</a>
                            </div>',
    'test_03'   => '                            <div class="itemFrase">
                                <h2 class="blueText">...that magic yes exists and exists inside of me.</h2>
                            </div>
                            <div class="itemText">
                                <p>For me, living this beautiful magic woman journey with Azul was a deep journey inward. Remembering little by little who I am before they told me who I was, and in this dance of my healthy masculine and my feminine, to find that love that I feel for myself, for my most authentic essence.</p>
                                <a href="#mercedes" class="sectionPerson">Read More</a>
                            </div>',

    'test_04'   => '<div class="itemFrase">
                        <h2 class="blueText">... For me it’s been a great gift, the opportunity to connect to myself...</h2>
                    </div>
                    <div class="itemText">
                        <p>Anaites way of expressing herself, her voice, her way of moving, made me open up to receive the magic you share, it is a moment of wonder each time, which excites me even more because I get it.</p>
                        <a href="#ana_brenda" class="sectionPerson">Read More</a>
                    </div>',
    'test_05'   => '<div class="itemFrase">
                        <h2 class="goldenText">has made me take the revolution inside and from there recognize that my reality</h2>
                    </div>
                    <div class="itemText">
                        <p>By her side I’ve remembered that I’m eternally love and consciousness and as such the most magical experience in this existence is to be human, that’s why I’ve come to this earth.</p>
                        <a href="#ana_fer_islas" class="sectionPerson">Read More</a>
                    </div>',
    'test_06'   => '<div class="itemFrase">
                        <h2 class="blueText">...She is a portal  of wisdom and  empowerment</h2>
                    </div>
                    <div class="itemText">
                        <p>I had been very disconnected from the feminine, from my vulnerability and my capacity to receive.</p>
                        <a href="#isabel" class="sectionPerson">Read More</a>
                    </div>',    // --------------

    // INSTAGRAM
    'instagramHeader'  => '<h2>
                                “The important thing is to give ourselves what <span>are we</span> looking for”
                            </h2>
                            <h3>
                                - Azul Anaité
                            </h3>
                            <p>
                                Follow the magic through our social networks
                            </p>',
    'formHeader'        => '<h2>Contact</h2>
                            <p>Subscribe</p>',
    // ---------------

    // CONTACTO
    'nombre_field'  => 'Name:',
    'correo_field'  => 'Email:',
    'celular_field' => 'Phone:',
    'mensaje_field' => 'Message:',
    'enviar_boton'  => 'Send',
    // -----------------

    // ADN
    'adn_portada' => '<img src="/web/home/img/adn01_en.png?var='.$numeros.'" alt="">',
    'adn_header'  => '<img class="show-on-large hide-on-med-and-down" src="/web/home/img/adn02.png?var='.$numeros.'" alt="">
                            <h3>All that to wish to be you already are?, we just need to light up the path.</h3>
                            <div class="show-on-medium-and-down hide-on-large-only">
                                <img src="/web/home/img/adn02.png?var='.$numeros.'" alt="">
                            </div>',

    'adn_info_01'  => '<p>I was born in 1976 in Guatemala, surrounded by volcanoes, rituals and colors.From a young age I was able to listen to the lakes and talk to the the stars.</p>
    <p>Later on I arrived in Mexico, the country that watched me grow; I always dreamed of ancient lands, I could remember details about Atlantis, I had very deep certainties... <span>Today my heart pulses and connects me with all the light beings that come from the sun behind the sun, I’m here to remind you of who you are, so you can become a brilliant reflection of that being that duels inside you.</span></p>',

    'adn_info_02'  => '<p>I was given the Divine Nature Activation as a gift for humanity, a powerful tool that opens, awakens and activates your heart, the core of each cell and DNA so
   you can live a fulfilled life. I love to perform activation rituals where together we can go back to the source; we travel in time to be that we have always been. I
   work synchronizing the divine (mind) the astral (heart) and the physical  (body) to awaken the sleeping sun within. All that to wish to be you already are?, we just need to light up the path.</p>
                            <div class="adnContentImg show-on-medium-and-down hide-on-large-only">
                                <img src="/web/home/img/adn03.png?var='.$numeros.'" alt="">
                            </div>
                            <h4>Luminous blessings to each heart that awakens.</h4>',
    // -------------

    // ALQUIMIA CACAO
    'alquimia_portada' => '<img src="/web/home/img/alquimia01_en.png?var='.$numeros.'" alt="">',

    'alquimia_info_01' => '<p> This training is a distillate of 8 years of work with cacao and many more years of work with plants , different healing techniques and ancestral wisdom.</p>
                            <div class="alquimiaImgsRight show-on-medium-and-down hide-on-large-only">
                                <img src="/web/home/img/alquimia02.png?var='.$numeros.'" alt="">
                            </div>
                            <p>This process is more than a workshop, it is <span>an invitation to start a journey of inner exploration into our interior, to find the would we hide within and the gifts we came to awaken in this life.</span></p>',

    'alquimia_info_02' => '<p>We live in a time of change, with moments that awaken deep emotions. This is a call to develop the inner resources we have to heal ourselves so we can accompany others in their own healing process.</p>
                            <p>The intention of this training is to accompany you in awakening your inner resources so you can open your eyes to the deep truth; You are much more than what you think, and the world needs your medicine.</p>
                            <div class="alquimiaContentImg show-on-medium-and-down hide-on-large-only">
                                <img src="/web/home/img/alquimia03.png?var='.$numeros.'" alt="">
                            </div>
                            <h4 class="center-responsive">Thanks you for listening to this calling.</h4>',
    // -------------------------

    // MUJER MAGIA
    'mujer_magia_portada' => '<img src="/web/home/img/magia_portada_en.png?var='.$numeros.'" alt="">',
    'mujer_magia_info_01'   => '<p>It is a call for magicians, for those who are ready to regain their magic. Magic Woman is a way to awaken the power that awakens in you ... Do you recall all that you believed and knew when you were a child?</p>

                                <p>Let’s awaken your gifts once again and uncover the hidden gifts that women from your linage have left for you and the permission for success that men fromyour clan have left for you.</p>

                                <p>It is a three month process where we will go to activate the feminine principle, the masculine principle and the heart as a vortex of creation and healing, remembering that connection is access and access is healing.</p>',

    'mujer_magia_info_02'   => '<h2>Remembering that connection is access and access healing. </h2>

                                <p>If you are ready to uncover who you are and join the mission of creating a new time on earth, we are waiting for you, wizards from all around the world, united to create a new humanity.</p>

                                <h2>May your life become an altar, and each step be safe.</h2>',

    'mujer_magia_info_03'   => '<p>This is a process of opening up the magic that exists in each being, it is a new time where we  can remember how to surrender to love and create a new life. </p>

                            <p>Ahealing through the stars and the re-connection to your guides y guardians, the initiation to your own medicine and life mission.</p>

                            <p>We will start with an activation of the feminine power, with not doing, learning to receive and nourish ourselves, and learning how to mother ourselves. Come discover your magic, it is so needed at this time.</p>

                            <h2>Are you ready to love and trust yourself?</h2>

                            <p>Light up your magic, heal yourself and discover the gifts your linage has for you.</p>

                            <h2>Unveil your beauty, learn to connect to nature, waken your feminine wisdom and take your place on earth.</h2>

                            <p>The basic principe of this work is how to give birth to ourselves, one and a housand times, as many times as necessary, to know that nothing is static, that all can transform, that our gaze creates that which we see, that our senses are much more than we have been told, that we are, as it has been said, magical beings of silent wisdom.</p>',
    // ------------------------

    // SHAKTI CACAO
    'shakti_cacao_info_01'  => '<h2 class="blueTxt">A gestation process where women awaken new forms to recognize themselves, the women in their linage, the relationship they have with life and with the wisdom of nature.</h2>

                                <p>We will go through a 9 month process where we will remember our story, looking at it in new ways, discovering the gifts the most important women in our lives had and discovering unknown gifts within ourselves.</p>',

    'shakti_cacao_info_02'  => '<p>This training is an invitation not just to heal the bond with the most important relationships in your feminine linage and parts of yourself, it is also an invitation to discover the gifts and wisdom that existed in each one of your women so you can rewrite your story and embrace who you are.</p>

                                <h2>Be reborn, give birth to yourself in consciousness, awaken your capacity to heal and replenish yourself and discover yourself as co-creator of your reality.</h2>',

    'shakti_cacao_info_03'  => '<h2 class="titleContent">For 9 months we will go through different phases, recognizing and opening all the gifts the women from our life have brought to us.</h2>',
    'shakti_contact'        => 'Contact',
    // --------------------------

    // TESTIMONIO 01
    'test__01'  => '<p>The  accompaniment  that  Anaite  has  given  me  during  this  journey  inside  has been a gift filled wit magic.</p>
    <p>Sha has ten me by the hand to help me learn how trough softness, subtlety and the  presence  in  the  body  one  can  reach  the  deepest  parts  where  I  could recognize, name and embrace my darkness. When I could do this my inner light started shining, showing me the gifts that where hidden in this depths.</p>
    <p>I&#44;m  deeply  grateful  to  be  able  to  call  her  my  teacher,  trough  her  reflection, congruence and medicine, today I recognize the teacher within me.</p>',

    'test__02' => '<p>Dear  Anaite,  you  taught  me  trough  your  loving  example  that  being  a woman is not in conflict with being a mother, a wife a lover and manifesting the dreams of my heart, that a woman is in constant evolution, that she knows herself to be human, and it is from this that her force and passion emerge, she becomes wisdom and shares it.</p>
     <p>Everything I’ve found in this journey where trusses, I learned what a wizard is, what an alchemist is, that turning darkness into gold is what gives you mastery.</p>
     <p>We are all a portal between heaven and earth, we just have to ask for that which we need for we can give it to ourselves.</p>
     <p>She  is  authentic,  natural,  spontaneous,  she  has  the  gift  of  weaving  nets that connect others.🙏🏽🙏🏽🙏</p>',

    'test__03' => '<p>Living  this  beautiful  experience,  Woman  Magic  (Mujer  Magia)  with  Anaite  has been a profound journey into myself. I started remembering, little by little, who I was  before  I  was  told  who  I  should  be.  In  this  dance  between  my  healthy masculine and feminine I found the love I feel for myself, for my most authentic essence.</p>
    <p>As Anaite always says, “When you love something enough  that something starts showing  you  all  its  secrets”.  This  is  real.  Here  I  am,  uncovering  more  and  more secrets about myself.</p>
    <p>I  lived  all  this  experience  accompanied  by  my  mother  and  a  beautiful  tribe  of wizards, all of them where mirrors and together we could remember. Thanks you, beautiful  Anaite,  for  so  much,  for  teaching  me  that  magic  exists  and  it  can  be found within me.</p>',

    'test__04'  => '<p>For  me  it’s  been  a  great  gift,  the  opportunity  to  connect  to  myself,  to reconnect with my inner resources, of recognizing myself as a woman.</p>
    <p>Anaites way of expressing herself, her voice, her way of moving, made me open  up  to  receive  the  magic  you  share,  it  is  a  moment  of  wonder  each time, which excites me even more because I get it.</p>
    <p>Her sensitivity helped me to connect with mine, and I think that is what we need  today,  to  be  sensitive  and  loving  with  ourselves  and  with  what surrounds us, to feel and to realice we are connected. I would like to keep learning  tools  to  make  my  body  more  sensitive,  to  listen  to  myself,  to enjoy life in all its senses, to evolve trough joy.</p>
    <p>I hope we can open space for men to share this circles.</p>
    <p>Thank you. 💚🌻🌟</p>',

    'test__05'  => '<p>❤️ To walk accompanied by Anaite has made me take the revolution inside and from there recognize that my reality is a reflection of my interior, that the way I feel comes from how my being feels it has to react to life, allowing me in this way to establish a new relationship with my nervous system.</p>
    <p>In this way I’ve been able to reintegrate in myself the aspects which felt a lack of love, I learned to self parent so I could consciously co-create  a more loving present.</p>
    <p>By her side I’ve remembered that I’m eternally love and consciousness and as such the most magical experience in this existence is to be human, that’s why I’ve come to this earth.</p>
    <p>This is how little by little I’ve come back to my body, inhabiting and nourishing myself, recognizing the rivers in my veins, the mountains in my skin, the fire in inspiration, wind in my breath.</p>
    <p>The earth upholds my steps, the sun nourishes my inspiration, each day I feel safer, more welcome, more at home, remembering that as I feel complete I open to the beauty of being me.</p>',

    'test__06'  => '<p>For a long time I have lived as if I was frozen, disconnected from my body and emotions.</p>
    <p>I  had  been  very  disconnected  from  the  feminine,  from  my  vulnerability  and  my capacity to receive. I always had resistance to feeling vulnerable, I lived in auto pilot without listening to my body, wearing it down.</p>
    <p>Anaite has accompanied me in my introspection and transformation processes, showing me a path of contention,  wisdom,  selfceare  and  loving  patience.  She  is  a  portal  of  wisdom and  empowerment,  She  knows  how  to  empower  other  woman  with  out  taking their power away, she sought me that every experience in life can be a resource and a gift to be shared with others.</p>',
    // ----------
];
