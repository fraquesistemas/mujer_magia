<?php
$numeros = rand();

return [

    /*
    |--------------------------------------------------------------------------
    | PAGINA ESPAÑOL INICIO
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */


    // OPCIONES
    'inicio'    => 'Inicio',
    'mision'    => 'Misión',
    'biografia' => 'Biografía',
    'servicios' => 'Formaciones',
    'testimonio'=> 'Testimonios',
    'instagram' => 'Instagram',
    // ---------------

    // FOOTER
    'copyright' => '<p>Todos los derechos reservados<br>Diseño: Vive_Magenta<br>Programación: <a href="https://fraquesistemas.com.mx" target="_blank">Fraque Sistemas</a></p>',
    // -----------------

    'leer_mas'  => 'Leer Más',
    'mas'       => 'Más',
    'cerrar'    => 'Cerrar',
    'contacto'  => 'Contacto',

    // BANNERS
    'img_01'   => '<img src="/web/home/img/galeria/mujer_magia_galeria_01.jpg?var='.$numeros.'">',
    'img_02'   => '<img src="/web/home/img/galeria/mujer_magia_galeria_02.jpg?var='.$numeros.'">',
    'img_03'   => '<img src="/web/home/img/galeria/mujer_magia_galeria_03.jpg?var='.$numeros.'">',

    'txt_01'   => '<h2>Lista Para Recuperar Tu Magia</h2>
                            <p>
                                Despierta tus dones, descubre los tesoros escondidos qué hay en tu historia.
                            </p>',

    'txt_02'   => '<h2>Tengo un regalo para compartir.</h2>
                            <p>Una herramienta poderosa que abre, despierta y activa tu corazón, el núcleo de cada célula y el ADN, para asi vivir tu vida en plenitud.</p>',

    'txt_03'   => '<h2>Es una invitación a comenzar un viaje.</h2>
                            <p>Un viaje de exploración profundo hacia nuestro interior para encontrar las heridas que esconden, dentro de cada uno de nosotros, los dones que vinimos a despertar en esta vida.</p>',
    // -------------------

    // MISION
    'mision_header' => '<h2>Misión</h2>',
    'mision_text'   => '<p>Azul Anaité es mujer ceremonialista y ritualista, guardiana de lo sagrado femenino, comparte talleres, círculos y formaciones para <span>mujeres que deseen despertar su magia, encontrando los dones y regalos escondidos en su historia, así como recordar el camino hacia tomar a la naturaleza como guía en la vida.</span></p>',
    'mision_frase'  => '<h2>“Si amas algo lo suficiente, ese algo te mostrara <span>todos sus secretos”</span></h2>',

    'mision_mas_01' => '<div class="misionTextMore">
                    <p>Con su esposo crean <span class="nameCacao">Cacao Ancestral</span>, un espacio que nació gracias a su encuentro con el cacao ceremonial hace 8 años, semilla que les abre un camino de corazón y los acompaña en su vida como parte de su destino.
                    <p>Comparte junto a su esposo procesos de iniciación para aquellos que deseen despertar a su ceremonialista interior y compartir la medicina que existe en ellos, así como retiros <span>donde se va tejiendo, una nueva humanidad, donde la magia, la confianza, y la relación entre la naturaleza y los humanos va despertando cada vez mas.</span></p>
                </div>',

    'mision_mas_02' => '<div class="misionTextMore misionTextMore01">
        <p>Son Padres de una pequeña de 5 años, y juntos crian a los 3 primeros hijos de Anaité, la familia es su mayor escuela, ser pareja el entrenamiento constante, ambos tienen en el corazón el deseo de convertirse en Abuelos, guardianes de su familia y de la familia que se ha ido creando en su camino.</p>
        <p>Despertar la escucha profunda de la sabiduría de la Tierra, descubrir los dones y virtudes que existen en los seres humanos y en la humanidad, entretejidos con la magia que se crea en todos los vínculos que existen en el universo.</p>
        <p>Descubrir la historia que cada territorio guarda, contada directamente de la naturaleza, <span>recordar que todos tenemos una conexión directa con lo divino, y que nuestro camino es el bien mas sagrado y preciado que tenemos.</span></p>
    </div>',
    // ---------------------------

    // BIOGRAFIA
    'biografia_header'  => '<img class="biografiaTitle" src="/web/home/img/mujer01_biografiaTitle.png?var='.$numeros.'" alt="">
    <br>
    <p>Nace en Guatemala y crece en México. Sus padres siembran en ella el amor por la justicia, la cultura, la memoria ancestral y la vida. Se acerca a muchos Abuelos y Abuelas de diversas etnias.<span>Con Keith Wilson, su maestro, reconoce el camino del cacao, el sendero de regreso al corazón; y con Scott Harshman, de la tradición Ojibwase, se inicia en la enseñanza de la rueda de la medicina y la astrología de la Tierra.</span></p>
    <p>Da a luz a 4 hijos y con ellos se despierta como partera de la sabiduría.</p>
    <h2>Ha creado varios procesos de transformación, despertar y reconexión, donde acompaña a otros seres a despertar su magia compartiendo la medicina que vive en ellos.</h2>',

    'biografia_extra01'   => '<p>Es creadora de la técnica de Activación del Corazón; del proceso de gestación femenina “Enciende tu Sabiduría Femenina, Shakti Cacao”; de “El Camino de la Diosa”; del proceso “Mujer Magia, Tu Encuentro con lo Sagrado Femenino”; en conjunto con su esposo, de la formación “Alquimia y Cacao : El Camino del Corazón”. Mujer ceremonialista y ritualista, despierta el amor por la matriz, por la fertilidad, por la sabiduría de los ciclos y la conexión con la sabiduría de la Tierra, y el mundo invisible.</p>
    <p>Imparte formaciones para mujeres, para Magos y Magas, y viajes con corazón al Lago de Atitlán donde los humanos se encuentran con su destino.</p>
    <p>Traza un camino de regreso al cuerpo, después de recorrer la memoria del trauma en la humanidad.</p>',

    'biografia_extra02'   => '<h2>Ha acompañado a cientos de mujeres en el proceso de parirse a sí mismas, denacer como magas, curanderas y guardianas de su propia medicina.</h2>
    <p>Hay mucho mas de lo que imaginamos en este planeta y en nosotros, mi misión es mostrar nuevas rutas, nuevos caminos y nuevas maneras de vivir la vida.</p>',

    // ---------------------------

    // BLOG
    'blog_mujer_magia_portada'  => '<img class="logoMain" src="/web/home/img/mujer_magia_blog.png?var='.$numeros.'" alt="" srcset="">',
    'blog_mujer_magia'  => '<p>Es un llamado para magas, para aquellas que están listas para recuperar su magia. Mujer Magia es un camino a despertar el poder que hay en ti.</p>
    <h3 class="blogSubtitle01">Vamos a despertar tus dones, descubrir los tesoros ocultos que las mujeres de tu familia ha dejado para ti.</h3>',

    'blog_shakti'  => '<p>Un proceso de gestación donde las mujeres despiertan nuevas formas de reconocerse a si mismas, a las mujeres de su linaje, y a la relación que tienen con la vida y con la naturaleza.</p>
    <h3 class="blogSubtitle02">Es una invitación a volver a nacer, volver a darte a luz a ti misma en conciencia...</h3>',

    'blog_cacao_portada'  => '<img class="logoMain" src="/web/home/img/mujer_magia_blog_cacao.png?var='.$numeros.'" alt="" srcset="">',
    'blog_cacao'  => '<p>Esto es un llamado a desarrollar los recursos internos que tenemos para poder sanar las heridas que existen en nuestro interior para así acompañar a otros en su propio proceso de sanación.</p>
    <h3 class="blogSubtitle01 colorBrown">¡Gracias por escuchar el llamado!</h3>',

    'blog_adn_title'    => '<img src="/web/home/img/mujer_magia_ADN.png?var='.$numeros.'" alt="" srcset="">',
    'blog_adn'     => '<p>A través de rituales de Activación volvemos al origen. Sincronizando lo divino (mente), astral (corazón) y físico (cuerpo) para despertar al Sol dormido en tu interior.</p>
    <h3 class="blogSubtitle02 colorBeige">Todo aquello que deseas ser ya lo eres... solo necesitamos iluminar el camino.</h3>',
    // -----------

    // TESTIMONIOS
    'testimonio_header'     => '<h2>Testimonios</h2>',
    'test_01'   => '                            <div class="itemFrase">
                                <h2 class="blueText">... al nombrar y abrazar mi oscuridad, mi luz interior se asoma y alumbra todos los tesoros.</h2>
                            </div>
                            <div class="itemText">
                                <p>
                                    El acompañamiento que Azul me ha dado durante este viaje hacia adentro ha sido un regalo lleno de magia.
                                </p>
                                <a href="#constanza_lechuga" class="sectionPerson">Leer Más</a>
                            </div>',
    'test_02'   => '<div class="itemFrase">
                                <h2 class="goldenText">Que todos somos portal entre el cielo y la tierra, que todo lo que necesitemos solo tenemos que pedirlo</h2>
                            </div>
                            <div class="itemText">
                                <p>
                                   Querida Anaité, que me enseñaste desde tu amoroso ejemplo: Que el Ser Mujer, no esta reñido con ser madre, esposa, amante y manifestar los sueños del corazón.
                                </p>
                                <a href="#anais_ceballos" class="sectionPerson">Leer Más</a>
                            </div>',
    'test_03'   => '                            <div class="itemFrase">
                                <h2 class="blueText">...que la magia si existe y que existe dentro de mi.</h2>
                            </div>
                            <div class="itemText">
                                <p>
                                    Para mi, vivir este hermoso viaje de mujer magia con Azul fue un camino profundo hacia adentro. Recordando poco a poco quien soy antes de que me dijeran quien era, y en esta danza de mi masculino y mi femenino sano, encontrar ese amor que siento por mi, por mi esencia más auténtica.
                                </p>
                                <a href="#mercedes" class="sectionPerson">Leer Más</a>
                            </div>',

    'test_04'   => '<div class="itemFrase">
                        <h2 class="blueText">... Para mi ha sido un gran regalo, la oportunidad de conectar conmigo misma.</h2>
                    </div>
                    <div class="itemText">
                        <p>
                            Tu sensibilidad hace que conecte con la mia y creo que eso necesitamos hoy, ser sensibles y amorosos con nosotros y lo que nos rodea, sentirnos y reconocernos conectarnos.
                        </p>
                        <a href="#ana_brenda" class="sectionPerson">Leer Más</a>
                    </div>',
    'test_05'   => '<div class="itemFrase">
                        <h2 class="goldenText">me ha hecho llevar la revolución  adentro y desde ahí reconocer que mi realidad es un reflejo de mi interior</h2>
                    </div>
                    <div class="itemText">
                        <p>
                            A su lado he recordado que espíritu en amor y consciencia soy eternamente. Y por ende lo más mágico que existe en este experiencia es Ser Humana; a eso vengo a La Tierra.
                        </p>
                        <a href="#ana_fer_islas" class="sectionPerson">Leer Más</a>
                    </div>',
    'test_06'   => '<div class="itemFrase">
                        <h2 class="blueText">.... Ella es un portal de sabiduría y empoderamiento. </h2>
                    </div>
                    <div class="itemText">
                        <p>
                            Anaité me ha acompañado en mis procesos de introspección y transformación , mostrándome el camino desde la contención, la sabiduría, el auto cuidado y amor paciente.
                        </p>
                        <a href="#isabel" class="sectionPerson">Leer Más</a>
                    </div>',    // --------------

    // INSTAGRAM
    'instagramHeader'  => '<h2>
                                “Lo importante es darnos a <span>nosotras</span> mismas lo que buscamos”
                            </h2>
                            <h3>
                                - Azul Anaité
                            </h3>
                            <p>
                                Sigue la magia a través de nuestras redes sociales
                            </p>',
    'formHeader'        => '<h2>Contacto</h2>
                            <p>Suscribete</p>',
    // ---------------

    // CONTACTO
    'nombre_field'  => 'Nombre:',
    'correo_field'  => 'Email:',
    'celular_field' => 'Celular:',
    'mensaje_field' => 'Mensaje:',
    'enviar_boton'  => 'Enviar',
    // -----------------

    // ADN
    'adn_portada' => '<img src="/web/home/img/adn01.png?var='.$numeros.'" alt="">',
    'adn_header'  => '<img class="show-on-large hide-on-med-and-down" src="/web/home/img/adn02.png?var='.$numeros.'" alt="">
                            <h3>Todo aquello que deseas ser ya lo eres... solo necesitamos iluminar el camino.</h3>
                            <div class="show-on-medium-and-down hide-on-large-only">
                                <img src="/web/home/img/adn02.png?var='.$numeros.'" alt="">
                            </div>',

    'adn_info_01'  => '<p>Nací en 1976 en Guatemala, rodeada de volcanes, ritos y colores, desde pequeña escuche a los lagos y hable con las estrellas.</p>
    <p>Llegue después a este México profundo que me ha visto crecer, desde siempre soñaba con Tierras Antigua, y recordaba cosas sobre la Atlántida, tenia certezas muy profundas... <span>hoy mi corazón pulsa y me conecta con todos los seres de luz que vienen del Sol detrás del Sol, estoy aquí para recordarte quien eres, para ser un reflejo luminoso de ese ser que yace en tu interior.</span></p>',

    'adn_info_02'  => '<p>Me han dado la Activación de la Divina Naturaleza como un regalo para la humanidad, una herramienta poderosa que abre, despierta y activa tu corazón, el núcleo de cada célula y el ADN, para asi vivir tu vida en plenitud; amo realizar rituales de Activación donde juntos volvemos al origen, viajamos en el tiempo para ser esa mariposa que siempre hemos sido. Trabajo sincronizando lo divino (mente), astral (corazón) y físico (cuerpo) para despertar al Sol dormido en tu interior. Todo aquello que deseas ser ya lo eres... solo necesitamos iluminar el camino.</p>
                            <div class="adnContentImg show-on-medium-and-down hide-on-large-only">
                                <img src="/web/home/img/adn03.png?var='.$numeros.'" alt="">
                            </div>
                            <h4>Bendiciones luminosas a cada corazón que despierta.</h4>',
    // -------------

    // ALQUIMIA CACAO
    'alquimia_portada' => '<img src="/web/home/img/alquimia01.png?var='.$numeros.'" alt="">',

    'alquimia_info_01' => '<p>Alquimia y Cacao es el destilado de casi 8 años de trabajo con el cacao y muchos mas años de trabajo con las plantas, diferentes técnicas de sanación y conocimientos ancestrales.</p>
                            <div class="alquimiaImgsRight show-on-medium-and-down hide-on-large-only">
                                <img src="/web/home/img/alquimia02.png?var='.$numeros.'" alt="">
                            </div>
                            <p>Este proceso es mas que un curso, es una <span>invitación a comenzar un viaje de exploración profundo hacia nuestro interior para encontrar las heridas que esconden, dentro de cada uno de nosotros, los dones que vinimos a despertar en esta vida.</span></p>',

    'alquimia_info_02' => '<p>Vivimos en tiempos de cambio, momentos que despiertan emociones profundas. Esto es un llamado a desarrollar los recursos internos que tenemos para poder sanar las heridas que existen en nuestro interior para así acompañar a otros en su propio proceso de sanación.</p>
                            <p>La intención de alquimia y cacao es acompañarte en el proceso de despertar tus recursos internos. Que abras los ojos a una verdad profunda; Que eres mucho más de lo que imaginas y qué el mundo necesita tu medicina.</p>
                            <div class="alquimiaContentImg show-on-medium-and-down hide-on-large-only">
                                <img src="/web/home/img/alquimia03.png?var='.$numeros.'" alt="">
                            </div>
                            <h4 class="center-responsive">Gracias por escuchar el llamado. </h4>',
    // -------------------------

    // MUJER MAGIA
    'mujer_magia_portada' => '<img src="/web/home/img/magia_portada.png?var='.$numeros.'" alt="">',
    'mujer_magia_info_01'   => '<p>Es un llamado para magas, para aquellas que estan listas para recuperar su magia. Mujer Magia es un camino a despertar el poder que despierta en ti... ¿Recuerdas en todo lo que creias y sabias cuando eras pequen@?</p>

                                <p>Vamos a despertar nuevamente tus dones, a descubrir los tesoros ocultos que las mujeres de tu familia dejaron en ti y el permiso al éxito que los hombres de tu clan te regalan.</p>

                                <p>Es un proceso de tres meses, donde iremos a activar el principio femenino, el principio masculino y el corazón como vórtice de creación y sanación.</p>',

    'mujer_magia_info_02'   => '<h2>Recordando que conexión es acceso y acceso sanación.</h2>

                                <p>Si estas lista para descubrir quién eres y unirte a la misión de crear un nuevo tiempo en la tierra, te estamos esperando, magos y magas de todo el mundo, reunidos para sembrar una nueva humanidad.</p>

                                <h2>Que tu vida se convierta en un Altar y cada paso sea sagrado.</h2>',

    'mujer_magia_info_03'   => '<p>Este es un proceso de apertura de la magia que existe en cada ser, es un nuevo tiempo, donde podemos recordarnos como rendirnos al amor, y crear una nueva vida.</p>

                            <p>Una sanación a través de las Estrellas y la re conexión con tus guías y guardianes, la iniciación a tu propia medicina y misión de vida.</p>

                            <p>Iniciamos con una Activación del Poder Femenino, el no hacer, el aprender a recibir y nutrirnos, así como convertirnos en nuestra propia Madre, a descubrir tu magia, lo necesitamos tanto en este tiempo.</p>

                            <h2>¿Estas listas para amarte y confiar en ti?</h2>

                            <p>Enciende tu magia, sánate y conoce los regalos que tu linaje tiene para ti.</p>

                            <h2>Revela tu belleza, aprende a conectarte con la naturaleza, despierta tu sabiduría femenina y toma tu lugar en la Tierra.</h2>

                            <p>Mi propuesta básica es aprender como darnos a luz, una y mil veces, las que sean necesarias, saber que nada es estático, que todo puede transformarse, que nuestra mirada crea lo que observamos, que nuestros sentidos son mucho mas de lo que nos han contado, que somos como bien se ha dicho, seres mágicos de sabiduría silenciosa </p>',
    // ------------------------

    // SHAKTI CACAO
    'shakti_cacao_info_01'  => '<h2 class="blueTxt">Un proceso de gestación donde las mujeres despiertan nuevas formas de reconocerse a si mismas, a las mujeres de su linaje, y a la relación que tienen con la vida y con la naturaleza.</h2>

                                <p>A través de un recorrido de 9 meses recordamos nuestra historia, mirándola a través de nuevas formas, descubriendo a las mujeres más importantes de nuestra vida y a nosotras mismas con los regalos y dones que no conocíamos.</p>',

    'shakti_cacao_info_02'  => '<p>Enciende tu sabiduría femenina es una invitación no solo a sanar tu vinculo con las relaciones mas importantes de tu linaje femenino y partes de ti misma, sino a descubrir los regalos, dones y sabiduría que existen en cada una de tus mujeres para poder reescribir tu historia y abrazar quien eres.</p>

                                <h2>Es una invitación a volver a nacer, volver a darte a luz a ti misma en conciencia, y despertar tu capacidad de sanarte y regenerarte, ademas de reconocerte como co-creadora de tu realidad.</h2>',

    'shakti_cacao_info_03'  => '<h2 class="titleContent">Durante 9 meses iremos por distintas facetas, reconociendo y abriendo todos losregalos que las mujeres de nuestra vida nos han traído.</h2>',
    // --------------------------

    // TESTIMONIO 01
    'test__01'  => '<p>El acompañamiento que Azul me ha dado durante este viaje hacia adentro ha sido un regalo lleno de magia.</p>
    <p>De su mano he aprendido como a través de la suavidad, la sutileza y la presencia en el cuerpo se puede llegar a lo más profundo, en donde al poder reconocer, nombrar y abrazar mi oscuridad, mi luz interior se asoma y alumbra todos los tesoros dentro de esas profundidades.</p>
    <p>Agradezco profundamente poder llamarla mi maestra, a través de su reflejo, congruencia y medicina hoy reconozco a la maestra en mi.</p>',

    'test__02' => '<p>Querida Anaité, que me enseñaste desde tu amoroso ejemplo: Que el Ser Mujer, no esta reñido con ser madre, esposa, amante y manifestar los sueños del corazón.</p>
    <p>Que una mujer esta en constante evolución, que se sabe humana, y de ahí viene su fuerza y su pasión y lo transmite, sabe convertirlo en enseñanza y lo comparte./<p>
    <p>Que todo lo que trae del viaje son puros tesoros, que es una maga, una alqumista que convirtió toda su oscuridad en oro, eso es lo que te da maestría.</p>
    <p>Que todos somos portal entre el cielo y la tierra, que todo lo que necesitemos solo tenemos que pedirlo, porque nosotras podemos darnoslo.</p>
    <p>Ella es auténtica, natural espontánea y tiene el don de tejerse en redes y conectar a otr@s a esas redes.🙏🏽🙏🏽🙏🏽</p>',

    'test__03' => '<p>Para mi, vivir este hermoso viaje de mujer magia con Azul fue un camino profundo hacia adentro. Recordando poco a poco quien soy antes de que me dijeran quien era, y en esta danza de mi masculino y mi femenino sano, encontrar ese amor que siento por mi, por mi esencia más auténtica.</p>
    <p>Y así, como dice Azul “cuando amas algo lo suficiente, ese algo te mostrará todos sus secretos” pues es real, aquí estoy descubriendo más y más secretos de mi ser.</p>
    <p>Y todo esto, lo viví acompañada por mi mamá y una tribu bellísima de magas, en el cual todas fuimos espejos y juntas recordamos. Gracias Azul hermosa por tanto y por enseñarme que la magia si existe y que existe dentro de mi.</p>',

    'test__04'  => '<p>Para mi has sido un gran regalo, la oportunidad de conectar conmigo misma , de reconocer mis recursos internos, de reconocerme mujer.</p>
    <p>Tu forma de expresarte, tu tono de voz, tu forma de moverte, hace q yo me abra y reciba la magia que compartes, es un wow cada vez y ademas me emociona porque lo comprendo.</p>
    <p>Tu sensibilidad hace que conecte con la mia y creo que eso necesitamos hoy, ser sensibles y amorosos con nosotros y lo que nos rodea, sentirnos y reconocernos conectarnos.</p>
    <p>Me gustaria seguir aprendiendo herramientas para sensibilizarme con mi cuerpo, escucharme y gozar  la vida en todos los sentidos, como dijeron arriba evolucionar a travez del goce.</p>
    <p>Me gustaria que el proyecto pueda ser para hombres y mujeres, que sea modalidad virtual.</p>
    <p>Graciassss!!!💚🌻🌟</p>',

    'test__05'  => '<p>❤️ Caminar acompañada por Anaite me ha hecho llevar la revolución  adentro y desde ahí reconocer que mi realidad es un reflejo de mi interior.</p>
    <p>Que cómo me siento viene de cómo mi ser siente que debe reaccionar ante la vida, permitiéndome así entablar una relación nueva con mi sistema nervioso central.</p>
    <p>Y así voy recuperando a las etapas de mi que se sintieron faltas de amor, re maternandome y reparentandome para conscientemente co crear un presente más amoroso.</p>
    <p>A su lado he recordado que espíritu en amor y consciencia soy eternamente. Y por ende lo más mágico que existe en este experiencia es Ser Humana; a eso vengo a La Tierra.</p>
    <p>Así pasó a paso regreso a mi cuerpo habitándome y nutriéndome, reconociendo los ríos en mis venas, las montañas en mi piel, el fuego en inspiración, el viento en mi aliento.</p>
    <p>La tierra sostiene mis pasos y El sol nutre mi inspiración. Cada día más segura, más recibida, más en casa. Recordando que al sentirme completa abro paso a la belleza de ser yo.</p>',

    'test__06'  => '<p>He vivido congelada todo este tiempo, desconectada de mi cuerpo y de mis emociones. Estuve muy desconectada con lo femenino, la vulnerabilidad y el recibir.</p>
    <p>Siempre tuve resistencia con ser vulnerable y viví en autopiloto sin escuchar mi cuerpo hasta desgastarlo.</p>
    <p>Anaité me ha acompañado en mis procesos de introspección y transformación , mostrándome el camino desde la contención, la sabiduría, el auto cuidado y amor paciente.</p>
    <p>Ella es un portal de sabiduría y empoderamiento. Ella sabe como empoderar a otras mujeres sin quitarles su poder y aceptar que toda experiencia de vida se puede transformar en un recurso y un regalo para ser compartido con otros.</p>',
    // ----------
];
