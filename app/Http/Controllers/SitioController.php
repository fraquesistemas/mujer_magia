<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Validator;
use Mail;

class SitioController extends Controller
{
    // VARIABLES GENERALES
    public function __construct()
    {
      if (!\Session::has('lang'))
          \Session::put('lang', 'es');

    }
  
    // ENVIAR CORREO
    public function sendMail(Request $request) {
      $messages = [
        'nombre.required'   => 'Este campo es Obligatorio',
        'correo.required'   => 'Este campo es Obligatorio',
        'correo.email'      => 'Formato de Email Incorrecto',
        'telefono.required' => 'Este campo es Obligatorio',
      ];

      $validator = Validator::make($request->all(), [
        'nombre'       => 'required',
        'correo'       => 'required|email',
        'telefono'     => 'required'
      ], $messages);

      if ($validator->fails()) {
        // dd($validator);
        $data = array(
          'exito'    => false
        );

        return $data;
      }

      $contact              = $request->all();
      $contact['fecha']     = date('Y/m/d h:i');
      $asunto               = '[ '.$contact['fecha'].' ] : Nuevo mensaje desde el sitio de Azul Anaité';


      try {
        // Enviar A CONTACTO DOCTOR ERICK
        \Mail::send('contacto_mail',$contact,function ($message) use ($asunto) {
          $message->subject($asunto);
          $message->to('shakticacao@gmail.com','Azul Anaité Contacto');
          $message->bcc('hola@fraquesistemas.com.mx','Fraque Test');
        });
        // -----------------------------------
      }
      catch (\ErrorException $e) {
        dd($e);
        $data = array(
          'exito'    => false
        );
        return $data;
      }

      $data = array(
        'exito'    => true
      );

      return $data;
    }
    // --------------  
}
